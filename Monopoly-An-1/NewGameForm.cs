﻿using Monopoly_An_1.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopoly_An_1
{
    public partial class NewGameForm : Form
    {

        List<Player> playerList = new List<Player>();

        public NewGameForm()
        {
            InitializeComponent();
        }


        private void startNewGameButton_Click(object sender, EventArgs e)
        {
           playerList.RemoveAll(x => x.Name.Trim() == string.Empty);
      
           GameForm gameForm = new GameForm(playerList);

           gameForm.ShowDialog();
 
        }

        private void InitializePlayerList()
        {
            playerList.Add(new Player { Name = "", Type = PlayerTypes.car, Money = Dice.Roll() });
            playerList.Add(new Player { Name = "", Type = PlayerTypes.dog, Money = Dice.Roll() });
            playerList.Add(new Player { Name = "", Type = PlayerTypes.hat, Money = Dice.Roll() });
            playerList.Add(new Player { Name = "", Type = PlayerTypes.iron, Money = Dice.Roll() });
            playerList.Add(new Player { Name = "", Type = PlayerTypes.shoe, Money = Dice.Roll() });
            playerList.Add(new Player { Name = "", Type = PlayerTypes.thimble, Money = Dice.Roll() });
            playerList.Add(new Player { Name = "", Type = PlayerTypes.wheelbarrow, Money = Dice.Roll() });

        }

        private void NewGameForm_Load(object sender, EventArgs e)
        {
            InitializePlayerList();
            playersGrid.DataSource = playerList;

        }
    }
}
