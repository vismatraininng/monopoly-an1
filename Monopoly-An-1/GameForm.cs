﻿using Monopoly_An_1.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopoly_An_1
{
     partial class GameForm : Form
    {
        public MonopolyGame game = new MonopolyGame();
        
        public GameForm()
        {
            InitializeComponent();
        }

      public GameForm(List <Player> playerList)
        {
            
            InitializeComponent();
          game.PlayerList = playerList;

          game.Run();

          ShowTable(game);
        }

      private void ShowTable(MonopolyGame game)
      {
          foreach (var cell in game.Table)
          {
              CellView cellView = new CellView();
              cellView.Left = cell.X;
              cellView.Top = cell.Y;
              cellView.Nume.Text = cell.Name;
              cellView.Price.Text = cell.Price.ToString();
              cellView.BackColor = cell.Color;

              cellView.Tag = cell;
              cellView.Click += ClickOnCell;

              Controls.Add(cellView);
          }
      }

      private void ClickOnCell(object sender, EventArgs e)
      {
          if (((MouseEventArgs)e).Button == System.Windows.Forms.MouseButtons.Left)
          {
              Console.WriteLine("Click pe " + ((Cell)((CellView)sender).Tag).Name);
          }
      }


        private void rollButton_Click(object sender, EventArgs e)
        {

        }

        private void GameForm_Load(object sender, EventArgs e)
        {

            playersGrid.DataSource = game.PlayerList;
            cellGrid.DataSource = game.Table;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add(label1.Text);
        }

        private void label1_MouseDown(object sender, MouseEventArgs e)
        {
            DragDropEffects dde1 = DoDragDrop(label1, DragDropEffects.All);
        }

        private void listBox1_DragDrop(object sender, DragEventArgs e)
        {
            listBox1.Items.Add(label1.Text);
        }

        private void listBox1_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }
    }
}
