﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopoly_An_1.Entities
{
     public class Player
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private int money;

        public int Money
        {
            get { return money; }
            set { money = value; }
        }
        private PlayerTypes type;

        public PlayerTypes Type
        {
            get { return type; }
            set { type = value; }
        }

        private int position;

        public int Position
        {
            get { return position; }
            set { position = value; }
        }
    }
}
