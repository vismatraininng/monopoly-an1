﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopoly_An_1.Entities
{
    public class Dice
    {
        static Random randomValue = new Random();
        static public int Roll() 
        {
            return randomValue.Next(1,7);
        }
    }
}
