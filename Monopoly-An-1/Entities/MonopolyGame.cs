﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopoly_An_1.Entities
{
     public class MonopolyGame
    {
        public List<Player> playerList = new List<Player>();

        public List<Player> PlayerList
        {
            get { return playerList; }
            set { playerList = value; }
        }


        public List<Cell> table = new List<Cell>();

        public List<Cell> Table
        {
            get { return table; }
            set { table = value; }
        }

        Dice dice = new Dice();

        public Dice Dice
        {
            get { return dice; }
            set { dice = value; }
        }

        void Init() 
        {
            TableInit();
        }

        private void TableInit()
        {
            Random randomNumber = new Random();

            for (int i = 1; i <= 5; i++)
            {
                Cell tempCell = new Cell()
                {
                    Name = "ramada " + i.ToString(),
                    Owner = null,
                    Price = randomNumber.Next(10, 50),
                    Rent = 100,
                    X = 10+102*i,
                    Y = 100,
                    Color = Color.FromArgb(255,
                        randomNumber.Next(50, 255),
                        randomNumber.Next(50, 255),
                        randomNumber.Next(50, 255)
                    )
                };

                Table.Add(tempCell);
            }
        }

         public void Run()
         {
             Init();
             //while (true)
             //{
             //    Move(player);
             //    if (Winner(player))
             //    {
             //        Console.WriteLine("Gata jocu");
             //    }
             //    player = NextPlayer(player);
             //}
         }
    }
}
